<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="33"/>
        <source>About</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="38"/>
        <source>What&apos;s %1?</source>
        <translation>Was ist %1?</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="34"/>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="35"/>
        <source>Copyright:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="36"/>
        <source>License:</source>
        <translation>Lizenz:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="37"/>
        <source>Source Code:</source>
        <translation>Quellcode:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="40"/>
        <source>&lt;p&gt;An Air Traffic Control inspired task manager for Sailfish OS.&lt;/p&gt;
&lt;p&gt;
In good old analog times, Air Traffic Control used a simple physical object
to manage dynamic changes in things to be scheduled, and ensuring proper and
strict ordering, which being flexible about the managed information itself.
&lt;/p&gt;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="61"/>
        <source>
&lt;p&gt;
It is attractively simple: you have a slanted board, and a couple of metal or
plastic slates, which are placed on this board forming a column
or stack. The slates are free to slide vertically.
&lt;/p&gt;
&lt;ul&gt;
    &lt;li&gt; Information about the thing to be scheduled is printed on a paper strip which is attached to a slate
    &lt;li&gt; New things are added to a new slate which is placed at the top of the column
    &lt;li&gt; Finished things are taken out of the stack by moving the slate sideways out of the column), causing the remaining slates to slide down
    &lt;li&gt; Gravity acts as the prioritizing agent
&lt;/ul&gt;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="77"/>
        <source>How to Use</source>
        <translation>Anwendung</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="79"/>
        <source>
&lt;p&gt;
Load a TODO.txt file from somewhere on your device (Documents) to populate the scheduling board.
&lt;/p&gt;
&lt;p&gt;
Once the stack has been populated, long-press on a slate to initiate dragging.
Drag to the left and up to shuffle it to the top. Drag to the
right and down to shuffle it to the end.

Use these actions to bring your list into the correct order.
&lt;/p&gt;
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="35"/>
        <source>About</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="35"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="36"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="37"/>
        <source>Load Todo List</source>
        <translation>Todoliste laden</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="50"/>
        <source>Load/Append Test File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="54"/>
        <source>Clear Board</source>
        <translation>Bord leer räumen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="55"/>
        <source>Export Events</source>
        <translation>Ereignisse exportieren</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="65"/>
        <source>Export Events (all-in-one)</source>
        <translation>Ereignisse exportieren (all-in-one)</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="72"/>
        <source>Scheduling Board</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="230"/>
        <source>No tasks yet</source>
        <translation>Noch keine Todos</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="231"/>
        <source>Pull down to add tasks</source>
        <translation>Ziehen zum Hinzufügen</translation>
    </message>
</context>
</TS>
