#!/bin/sh

# SPDX-FileCopyrightText: © 2023 Peter G. (nephros)
#
# SPDX-License-Identifier: CC0-1.0

# this script is intended to build the application directly on-device during development/testing.
# Other build methods should be used to make release builds.

printf 'specifying...\n'
specify -Nns rpm/*yaml >/dev/null || exit 1
#if [[ ! x"$1" = x"release" ]] ; then
#  gtag=$(git rev-parse --short HEAD).$(git branch --show-current | sed 's/-/_/')
#  sed -i -e "s/\(Release: \).*$/\1 0\+$gtag/" rpm/*.spec
#fi
_libdir=/usr/lib
if [ $(getconf LONG_BIT) -eq 64 ]; then
  _libdir=/usr/lib64
fi
printf 'linting...\n'
for f in $(find qml -name "*.qml" ); do
 ${_libdir}/qt5/bin/qmllint "$f" || exit 1
done
for f in $(find qml -name "*.js" ); do
 ${_libdir}/qt5/bin/qmllint "$f"
done
printf 'building...\n'
rpmbuild -bb --build-in-place rpm/*.spec > build.log 2>&1
printf "exit: $?\n"
awk '/^Wrote/ {print $NF}' build.log
#if [[ ! x"$1" = x"release" ]] ; then
#  git restore rpm/*spec > /dev/null
  printf 'installing...\n'
  awk '/^Wrote/ {print $NF}' build.log | xargs xdg-open
#else
#  git tag -f release-build
#fi
