<!--
SPDX-FileCopyrightText: © 2023 Peter G. (nephros)

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Flight Strip

An Air Traffic Control inspired task manager for Sailfish OS.

## Concept

**TL;DR** We manage TODO lists and add entries on a calendar for them.

### The Issue

Being a man of diverse and plentiful interests leads to several personal (and
in theory also professional) projects, which lead to a collection of tasks that
need doing.

Personal and professional life generates blocks of time which are allocated,
and others which are yet free to be allocated.

But both task collections and time slots are bound to change at any moment, due
to many reasons only some of which can be controlled.

TODO lists (such as todo.txt and others) help collecting, categorizing, and
prioritizing such tasks.  But they are not very good at actually *scheduling*
them.

On the other hand  calendars (such as iCalendar-driven applications) are good
at scheduling one-time or repeating events. But they usually are not great when
several things change schedule at the same time, and often fail at
prioritizaton.

### The Air Traffic Controller's Scheduling board

In good old analog times, Air Traffic Control used a simple physical object to
manage dynamic changes in things to be scheduled, and ensuring proper and
strict ordering, which being flexible about the managed information itself.

See: [Flight Strip](https://en.wikipedia.org/wiki/Flight_progress_strip)

(Preface: the author is in fact not in the least knowledgeable in this area,
and uses the things described here purely as inspiration and illustration, with
no pretense that they are accurate or even remotely true.)

Very simple: you have a slanted board, and a couple of metal or plastic slates,
which are placed on this board forming a column or stack. The slates are free
to slide vertically.

[Board](https://web.archive.org/web/20040701032847im_/http://www.gov.im/lib/images/airport/atc/fpsbay.jpg)

 - Information about the thing to be scheduled is printed on a paper strip which is attached to a slate
 - New things are added to a new slate which is placed at the top of the column
 - The "nextmost" or highest priority slate is at the *bottom* of the column (Programmers beware!)
 - Finished things are taken out of the stack by moving the slate sideways out of the column), causing the remaining slates to slide down
 - Gravity acts as the prioritizing agent

### Scheduling

The application acts as "middleware" between:

 - a Todo list (tasks)
 - "free" time which is reserved to be allocated to tasks  (time slots)
 - a calendar keeping the time slots

The basic workflow is:

 1. Time slots are identified and marked on the calendar (typically one or more recurring events are created).
 1. The list is placed on the board
 1. Items of the list are ordered using the Air Traffic Contoller's Scheduling board method
 1. Upon completion, the items on the list are allocated to the time slots

with the assumption that "time slots" are reserved and relatively static, and tasks are highly dynamic and may change order frequently and quickly.

The scheduling board method should help manage the sequence and content of tasks, ignoring *when* they are to be executed.

The time slots on the calendar ensure that there is time to do *something*, but do not know what the activity is going to be.

*Notes on allocation*

 1. Time slot marks
    - several "sets" of time slots may exist, (e.g. a single recurring calendar entry makes one set of slots).
    - slots may have a single "tag" associated
    - tasks may have one or more "tags" (e.g. the "project" and "context" entries of the todo.txt spec)
    - when allocating slots, task whose tags match the tag of the slot set are allocated to this slot set
 1. Already allocated time slots:
    - in simple implementations are overwritten with the new schedule
    - advanced implementations may do conflict resolution:
      - either by loading slots and mixing them in the list before it is placed on the board
      - or by mixing them according to priorities when saving, grouping tasks by priority
      - other, secondary factors (such as due dates or implementation-specific heuristics)

### Implementation

The PoC for the above ideas is implemented  as an application for Sailfish OS in QML, 
and strives to use as many native interfaces from the OS as is feasible.


## Licensing

The project strives to be compatible with compliant with version 3.0 of the [REUSE Specification](https://reuse.software/spec/)

### Implementation

The source code and implementation of the Flight Strip application is released
under the Mozilla Public License v2.0, except for the items noted below.

### Specification and Documentation

This README document, and other documents contributing to the specification of
Flight Strip, as well as any Documentation are released under the
[CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) license.

### Other Content

The above does not conver the following:

#### Fonts
The above does not include fonts included in packages of the application, which
have their own licensing terms.

#### Third-party software:

 - qml/js/todotxt.js: by Michael Roufa, MIT License, see https://github.com/roufamatic/todo-txt-js
 - FlapQML: Developed by *Christian Visintin*,  License: GPL v3, see https://github.com/veeso/FlapQML

