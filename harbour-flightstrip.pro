# SPDX-FileCopyrightText: © 2023 Peter G. (nephros)
#
# SPDX-License-Identifier: CC0-1.0

######################################################################
# This is for translation ONLY, use build.sh for building
######################################################################

TEMPLATE = aux
TARGET = harbour-flightstrip
CONFIG += sailfishapp sailfishapp_i18n

lupdate_only {
SOURCES += \
    qml/$${TARGET}.qml \
    qml/pages/AboutPage.qml \
    qml/cover/CoverPage.qml \
    qml/pages/MainPage.qml

}

# Input
TRANSLATIONS += translations/$${TARGET}-en.ts \
                translations/$${TARGET}-de.ts \
