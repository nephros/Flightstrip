// Copyright (c) 2022 Peter G. (nephros)
//
// SPDX-License-Identifier: MPL-2.0

import QtQuick 2.6
import Sailfish.Silica 1.0

//BackgroundItem { id: stripItem
//    height: Math.max(strip.height, Theme.itemSizeLarge)
ListItem { id: stripItem
    //height: Math.max(strip.height, Theme.itemSizeLarge)
    contentHeight: Math.max(strip.height, Theme.itemSizeLarge)
    //width: parent.width <-- not in a delegate!

    property bool dragged: false

    property var task: null
    onTaskChanged: {
        if (task != null) {
            checkState();
            //console.debug("got a task:", task.content, task.finished, task.dueDate)
            if (task.prio === "")
                task.prio = "-"
            // set whitespace strings so the BorderedLabels still display something
            if (task.contexts.length == 0)
                task.contexts = ["  "]
            if (task.projects.length == 0)
                task.projects = ["  "]
        }
    }

    function checkState() {
        if (task.finished) return;
        if (checkDue()) { state = "due"; return; }
        if (checkOverdue()) { state = "overdue"; return; }
    }
    function checkDue() {
        if (task.dueDate === 0) return false;
        const date = new Date(Number(task.dueDate * 1000));
        // is it today??
        const today = new Date(Date.now())
        //console.debug("checking today:", today)
        if (date.getDate() === today.getDate() && date.getMonth() === today.getMonth() && date.getFullYear() === today.getFullYear())
            return true
        // is it tomorrow?
        const tomorrow = new Date(today)
        tomorrow.setDate(tomorrow.getDate() + 1)
        tomorrow.setHours(0,0,0,0);
        //console.debug("checking tomorrow:", tomorrow)
        if (date.getDate() === tomorrow.getDate() && date.getMonth() === tomorrow.getMonth() && date.getFullYear() === tomorrow.getFullYear())
            return true
        return false
    }
    function checkOverdue() {
        if (task.dueDate === 0) return false;
        const date = new Date(Number(task.dueDate * 1000));
        // set to yesterday, midnight
        const yesterday = new Date(Date.now())
        yesterday.setDate(yesterday.getDate() - 1);
        yesterday.setHours(0,0,0,0);
        return date < yesterday;
    }

    states: [
        // this should be on the top so it matches first
        State { name: "finished"
            when: task.finished
            PropertyChanges { target: blinker; color: "green"; active: false; lightOn: true }
            PropertyChanges { target: finishedInfo; visible: true }
            PropertyChanges { target: stateInfo; text: qsTr("landed") }
        },
        State { name: "overdue"
            PropertyChanges { target: blinker; color: "red"; active: true }
            PropertyChanges { target: stateInfo; text: qsTr("delayed") }
            PropertyChanges { target: stripbg; border.color: Qt.tint("orange", Theme.secondaryColor) }
            //PropertyChanges { target: stripbg; border.width: 5; border.color: Qt.tint("orange", Theme.secondaryColor) }
            PropertyChanges { target: app; haveOverdue: true }
        },
        State { name: "due"
            PropertyChanges { target: blinker; color: "orange"; active: true }
            PropertyChanges { target: stateInfo; text: qsTr("on time") }
            //PropertyChanges { target: stripbg; border.width: 5; border.color: Qt.tint("yellow", Theme.secondaryColor) }
            PropertyChanges { target: stripbg;  border.color: Qt.tint("yellow", Theme.secondaryColor) }
            PropertyChanges { target: app; haveDue: true }
        }
    ]
    //onStateChanged: console.debug("task state changed:", state);

    Row { id: strip
        width: parent.width
        BorderedLabel{ id: prio
            text: task.prio
            height: Math.max(content.height, projects.height, dateInfos.height)
            width: Math.max(stripItem.width * 0.05, blinker.width, callsign.width)
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            Blinkers { id: blinker; anchors.horizontalCenter: parent.horizontalCenter ; anchors.bottom: parent.bottom }
            Label{ id: callsign ; text: task.addons.callsign;
                font.family: app.customFont ? app.customFontName : "mono"
                font.pixelSize: Theme.fontSizeTiny
                anchors.leftMargin: 2
                anchors.rightMargin: 2
                anchors.horizontalCenter: parent.horizontalCenter ; anchors.top: parent.top }
        }
        BorderedLabel { id: projects
            width: prio.width * 1.5
            //text: Theme.highlightText(task.projects.join(' '), "+", Theme.highlightColor)
            text: Theme.highlightText(task.projects.items.join(' '), "+", Theme.highlightColor)
            /*
                Repeater {
                    model: task.projects
                    delegate: Text { text: model.items.join(' ') }
                }
                */
        }
        Column{ id: content
            //width: Math.max(stripItem.width * 0.33, implicitWidth)
            width: Math.max(stripItem.width * 0.4)
            anchors.top: parent.top
            BorderedLabel{ id: todo ; text: task.content; width: parent.width
                height: Math.max(implicitHeight, stripItem.height - ctxs.height )
            }
            BorderedLabel{ id: ctxs ;
                //text: Theme.highlightText(task.contexts.join(' '), "@", Theme.highlightColor)
                text: Theme.highlightText(task.contexts.items.join(' '), "@", Theme.highlightColor)
                width: parent.width;
                verticalAlignment: Text.AlignBottom
                /*
                    Repeater {
                        model: task.contexts
                        delegate: Text { text: model.items.join(' ') }
                    }
                    */
            }
        }
        Column{ id: dateInfos
            //width: Math.max (stripItem.width * 0.2 , implicitWidth)
            width: Math.max(stripItem.width - (prio.width + projects.width +  content.width ), implicitWidth)
            BorderedLabel{ id: stateInfo
                width: prio.width * 2
                text: qsTr("")
            }
            BorderedLabel{ id: createdInfo
                visible: task.creationDate > 0
                width: prio.width * 2
                property date cd: new Date(Number(task.creationDate*1000))
                text: task.creationDate > 0 ? "D: " + cd.toISOString().substr(0,10) : ""
                font.pixelSize: Theme.fontSizeSmall
            }
            BorderedLabel{ id: finishedInfo
                visible: false
                width: prio.width * 2
                property date cd: new Date(Number(task.completionDate*1000))
                text: task.completionDate > 0 ? "A: " + cd.toISOString().substr(0,10) : ""
                font.pixelSize: Theme.fontSizeSmall
            }
            BorderedLabel{ id: dueInfo
                visible: task.dueDate > 0
                width: prio.width * 2
                property date dd: new Date(Number(task.dueDate*1000))
                text: task.dueDate > 0 ? "E: " + dd.toISOString().substr(0,10) : ""
                font.pixelSize: Theme.fontSizeSmall
            }
        }
    }
    Rectangle { id: stripbg
        z: -10
        clip: true
        anchors.fill: parent;
        border.width: 2
        border.color: Qt.tint("blue", Theme.secondaryColor)
        color: dragged ? Theme.rgba("gray", Theme.highlightBackgroundOpacity) : (Theme.rgba(((Theme.colorScheme == Theme.LightOnDark) ? "royalblue" : "oldlace"), Theme.highlightBackgroundOpacity) )
        Behavior on color { ColorAnimation{ duration: 200 } }
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
