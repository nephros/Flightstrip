// Copyright (c) 2022 Peter G. (nephros)
//
// SPDX-License-Identifier: MPL-2.0

import QtQuick 2.6
import Sailfish.Silica 1.0

Row {
    property color color: "gray"
    property bool active: false
    property bool lightOn: false
    onLightOnChanged: {
        l.dimmed = !lightOn
        r.dimmed = !lightOn
    }
    onActiveChanged: {
        l.busy = active
    }
    Timer { id: delayTimer
        interval: 100
        running: l.busy
        repeat: false
        onTriggered: {
           r.busy = true
        }
    }

    Blinker{id: l; color: parent.color }
    Blinker{id: r; color: parent.color }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
