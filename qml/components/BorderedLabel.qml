// Copyright (c) 2022 Peter G. (nephros)
//
// SPDX-License-Identifier: MPL-2.0

import QtQuick 2.6
import Sailfish.Silica 1.0

Label {
    wrapMode: Text.WordWrap
    elide: Text.ElideLeft

    font.family: app.customFont ? app.customFontName : "mono"
    //font.pixelSize

    //font.weight: Font.Light
    font.capitalization: app.customFontSC ? Font.SmallCaps : Font.MixedCase
    //font.capitalization: Font.AllUppercase
    //font.letterSpacing
    //font.wordSpacing
    //font.kerning
    //font.preferShaping
    //font.hintingPreference
    //font.styleName: "Mono"

    Rectangle {
        anchors.fill:parent
        z: 0
        border.width: 1
        border.color: Theme.secondaryColor
        color: "transparent"
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
