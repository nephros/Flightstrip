// Copyright (c) 2022 Peter G. (nephros)
//
// SPDX-License-Identifier: MPL-2.0

import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

CoverBackground {
    id: coverPage

    Image {
        source: "./background.png"
        anchors {
            left: parent.left
            bottom: parent.bottom
        }
        width: parent.width * 4/5
        fillMode: Image.PreserveAspectFit
        opacity: 0.33
    }
    Text { id: word1
        rotation: 90
        text: "flight"
        anchors.centerIn: parent
        font.pixelSize: Theme.fontSizeHuge
        horizontalAlignment: Text.AlignHCenter
        color: Theme.secondaryHighlightColor
    }
    Text {
        rotation: -90
        text: "strip"
        anchors.left: word1.horizontalCenter
        anchors.top: word1.verticalCenter
        //anchors.right: parent.right
        font.pixelSize: Theme.fontSizeHuge
        color: Theme.secondaryColor
    }
    Blinkers { id: blinkers
        visible: active
        active: cover.status == Cover.Active && (app.haveDue || app.haveOverdue)
        color: app.haveDue ? "orange" : (app.HaveOverDue ? "red" : Theme.secondaryColor)
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
    }
    Label {
        visible: blinkers.active
        font.pixelSize: Theme.fontSizeTiny
        text: "overdue tasks"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: blinkers.top
    }

    /*
        CoverActionList {
            CoverAction { iconSource: "image://theme/icon-m-sync";            onTriggered: {app.sheepModel.getNewData} }
            CoverAction { iconSource: "image://theme/icon-m-media-playlists"; onTriggered: {pageStack.push(Qt.resolvedUrl("VideoPage.qml"), {"playlist": sheepModel.getPlayList(), "generation": sheepModel.gen, "imgid": qsTr("all")} ) } }
        }
    */
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
