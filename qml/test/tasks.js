var data = [
  { "finished": false,	"prio": "A",	"completionDate": 0,		"dueDate": 1651214018,	"creationDate": 0,		"content": "Dinner with Joe and Judy",		"projects": { "items": ["+networking"]},					"contexts": { "items": ["@social", "@dans", "@food" ]}},
  { "finished": true,	"prio": "B",	"completionDate": 1651478317,	"dueDate": 1651405511,	"creationDate": 0,		"content": "get a Machine",			"projects": { "items": ["+work", "+domination"]},				"contexts": { "items": [ ]}},
  { "finished": false,	"prio": "C",	"completionDate": 1651478317,	"dueDate": 1651405511,	"creationDate": 1651214068,	"content": "learn French",			"projects": { "items": ["+work", "+domination"]},				"contexts": { "items": [ ]}},
  { "finished": false,	"prio": "C",	"completionDate": 0,		"dueDate": 1651475511,	"creationDate": 1651214068,	"content": "take over the World",		"projects": { "items": ["+studio", "+domination"]},				"contexts": { "items": [ ]}},
  { "finished": false,	"prio": "A",	"completionDate": 0,		"dueDate": 1651478317,	"creationDate": 1651214068,	"content": "Get my own moon, with Black Jack and hookers!" ,  "projects": { "items": ["+domination"]},				"contexts": { "items": ["@moon", "@hookers", "@black", "@jack", "@gambling", "@casino" ]}},
  { "finished": false,	"prio": "B",	"completionDate": 0,		"dueDate": 1651214018,	"creationDate": 0,		"content": "Dump Judy (maybe)",			"projects": { "items": ["+networking"]},					"contexts": { "items": ["@social", "@dating", "@hookers" ]}},
  { "finished": false,	"prio": "F",	"completionDate": 0,		"dueDate": 0,		"creationDate": 1651214068,	"content": "An Item Name with some more content than the others" ,  "projects": { "items": ["+project"]},			"contexts": { "items": [ ]}},
  { "finished": false,	"prio": "Z",	"completionDate": 0,		"dueDate": 0,		"creationDate": 0,		"content": "Another Boring Task",		"projects": { "items": ["+project3", "+project7", "+project54", "+project17"]}, "contexts": { "items": ["@Atag" ]}},
  { "finished": true,	"prio": "",	"completionDate": 0,		"dueDate": 0,		"creationDate": 1651214018,	"content": "A finished Item ",			"projects": { "items": ["+project6"]},						"contexts": { "items": ["@ATag" ]}},
  { "finished": false,	"prio": "",	"completionDate": 0,		"dueDate": 0,		"creationDate": 0,		"content": "A third Item, unfinished",		"projects": { "items": ["+project7"]},						"contexts": { "items": ["@AnotherTag" ]}}
];

// vim: ft=javascript ts=8 softtabstop=0 sw=0
