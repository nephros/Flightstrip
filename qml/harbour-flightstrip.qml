// Copyright (c) 2022 Peter G. (nephros)
//
// SPDX-License-Identifier: MPL-2.0

/*

This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
If a copy of the MPL was not distributed with this file, You can obtain one at
https://mozilla.org/MPL/2.0/.

*/

import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import Nemo.DBus 2.0
import "pages"
import "cover"
import "components"
import "fonts/fonts.js" as Fonts

ApplicationWindow {
    id: app

    property bool haveOverdue: false
    property bool haveDue: false
    property date today

    Component { id: wpImage
        Image {
            // silence some warnings
            property url imageUrl: source
            property string wallpaperFilter: ""
            source: Qt.resolvedUrl("images/bg_ls.jpg")
            fillMode: Image.PreserveAspectCrop
        }
    }
    background.wallpaper: wpImage

    onVisibleChanged:{
        today = new Date()
    }

    Component.onCompleted: {
        // for sailjail
        Qt.application.domain  = "sailfish.nephros.org";
        Qt.application.version = "unreleased";
        console.info("Intialized", Qt.application.name, "version", Qt.application.version, "by", Qt.application.organization );
        console.debug("Parameters: " + Qt.application.arguments.join(" "))
        // correct landscape for Gemini, set once on start
        allowedOrientations = (devicemodel === 'planetgemini')
            ? Orientation.LandscapeInverted
            : defaultAllowedOrientations
    }

    // correct landscape for Gemini
    ConfigurationValue {
        id: devicemodel
        key: "/desktop/lipstick-jolla-home/model"
    }
    ConfigurationGroup  {
        id: settings
        path: "/org/nephros/" + Qt.application.name
    }
    ConfigurationGroup  {
        id: config
        scope: settings
        path:  "app"
        property bool customFont: true // true: use provided custom font
        property int customFontFace: 0
        property bool gravity: true // true: pull to bottom
        property bool loadfinished: false 
    }
    property alias gravity: config.gravity
    property alias loadfinished: config.loadfinished

    property alias customFont: config.customFont
    property alias customFontFace: config.customFontFace
    property alias customFontName: vectorFont.name
    property bool customFontSC: Fonts.index[config.customFontFace].smallcaps
    FontLoader { id: vectorFont
        source: Qt.resolvedUrl(Fonts.index[config.customFontFace].filename);
    }

    DBusInterface { id: calendar
        service: "com.jolla.calendar.ui"
        path: "/com/jolla/calendar/ui"
        iface: "com.jolla.calendar.ui"
        function importIcs(icsString) {
            call("importIcsData", [icsString])
        }
    }

    initialPage: Component { MainPage{} }
    cover: Component {CoverPage{} }

    PageBusyIndicator { running: app.status === Component.Loading }

}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
