// SPDX-FileCopyrightText: © 2023 Peter G. (nephros)
//
// SPDX-License-Identifier: MPL-2.0

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */


/*
**** example VTODO string: *****
==========================
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//org.nephros.sailfish//NONSGML FlightStrip//EN
BEGIN:VTODO
DTSTAMP:20220130T134500Z
SEQUENCE:0
UID:uid4@example.com
DUE:20220415T235959
STATUS:NEEDS-ACTION
SUMMARY:Example TODO
END:VTODO
END:VCALENDAR
==========================
***** example VTODO string end *****
*/

function exportIcs(data, filename){

}
function convertTodoTxt(data) {
}

/*
 * Takes a ListModel object, returns a string containing an iCalendar VTODO Snippet
 *
 * element: object from Listmodel.get()
 */

function elementToVTODO(element) {
    var now = new Date(Date.now());
    var due = "";
    var alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var p = alphabet.indexOf(element.prio);
    var data = "BEGIN:VTODO\r\n"
        + "DTSTAMP:" + now.toISOString().substr(0,19).replace(/[-:.]/g,'') + 'Z'  + '\r\n'
        + "UID:" + encodeURIComponent(Qt.application.name) + "+" + element.addons.callsign + "@" + Qt.application.domain + '\r\n'
        + "SUMMARY:" + element.content + '\r\n';
    data += "STATUS:" + (( element.finished ) ? 'COMPLETED' : 'IN-PROCESS') + '\r\n';
    if (element.dueDate !=0) {
        var due = new Date(element.dueDate * 1000);
        due = due.toISOString().substr(0,10).replace(/-/g,'') + 'T235959Z'
        data += "DUE:" + due + '\r\n'
    };
    if (p > -1) { data += "PRIORITY:" + (p+1) + '\r\n'; }
    data += "END:VTODO\r\n";
    return data;
}

/*
 * Takes a VTODO element and wraps it in a correct iCalendar header
 * data: one or more VTODOs
 */
function wrapCal(data) {
    return 'BEGIN:VCALENDAR\r\n'
        + 'VERSION:2.0\r\n'
        + 'PRODID:-//org.nephros.sailfish//NONSGML FlightStrip//EN\r\n'
        + data
        + "END:VCALENDAR\r\n";
}

function putTodo(data, url) {
    var uidstr = Qt.md5(Math.floor(Math.random() * 1000) * Date.now());
    var path = url + '/' + uidstr + '.ics'
    var r = new XMLHttpRequest();
    r.open('PUT' , path);
    r.setRequestHeader('Content-Type', 'text/calendar; charset=utf-8')
    r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    r.onreadystatechange = function(event) {
      if (r.readyState == XMLHttpRequest.DONE) {
        console.debug("event written to " + path);
      }
    }
    r.send(wrapCal(data));
    return path;
}
// vim: filetype=javascript expandtab ts=4 st=4 sw=4
