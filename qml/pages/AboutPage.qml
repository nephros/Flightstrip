// Copyright (c) 2022 Peter G. (nephros)
//
// SPDX-License-Identifier: MPL-2.0

import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Page {
  id: about

  readonly property string copyright: "Peter G. (nephros)"
  readonly property string email: "mailto:sailfish@nephros.org?bcc=sailfish+app@nephros.org&subject=A%20message%20from%20a%20" + Qt.application.name + "%20user&body=Hello%20nephros%2C%0A"
  readonly property string license: "MIT-2.0"
  readonly property string licenseurl: "https://www.mozilla.org/en-US/MPL/2.0/"
  readonly property string source: "https://codeberg.org/nephros/flightstrip"

  SilicaFlickable {
    contentHeight: col.height + Theme.itemSizeLarge
    anchors.fill: parent
    VerticalScrollDecorator {}
    Column {
        id: col
        width: parent.width - Theme.horizontalPageMargin
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: Theme.paddingLarge
        PageHeader { title: qsTr("About") + " " + Qt.application.name + " " + Qt.application.version }
        DetailItem { label: qsTr("Version:");      value: Qt.application.version }
        DetailItem { label: qsTr("Copyright:");    value: copyright;                            BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(email) } }
        DetailItem { label: qsTr("License:");      value: license + " (" + licenseurl + ")";    BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(licenseurl) } }
        DetailItem { label: qsTr("Source Code:");  value: source;                               BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(source) } }
        SectionHeader { text: qsTr("What's %1?").arg(Qt.application.name) }
        HelpLabel {
          text: qsTr(
'<p>An Air Traffic Control inspired task manager for Sailfish OS.</p>
<p>
In good old analog times, Air Traffic Control used a simple physical object
to manage dynamic changes in things to be scheduled, and ensuring proper and
strict ordering, which being flexible about the managed information itself.
</p>
'
)
        }
        Image {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width * 3/4
            //source: Qt.resolvedUrl("https://media2.wnyc.org/i/0/350/c/99/photologue/photos/slide-jfk-150791.jpg")
            source: Qt.resolvedUrl("https://media.defense.gov/2016/Dec/15/2001679725/-1/-1/0/161212-F-IP756-009.JPG")
            asynchronous:true
            cache: false
            fillMode: Image.PreserveAspectFit
        }

        HelpLabel {
          text: qsTr(
'
<p>
It is attractively simple: you have a slanted board, and a couple of metal or
plastic slates, which are placed on this board forming a column
or stack. The slates are free to slide vertically.
</p>
<ul>
    <li> Information about the thing to be scheduled is printed on a paper strip which is attached to a slate
    <li> New things are added to a new slate which is placed at the top of the column
    <li> Finished things are taken out of the stack by moving the slate sideways out of the column), causing the remaining slates to slide down
    <li> Gravity acts as the prioritizing agent
</ul>
'
)
        }
        SectionHeader { text: qsTr("How to Use") }
        HelpLabel {
          text: qsTr(
'
<p>
Load a TODO.txt file from somewhere on your device (Documents) to populate the scheduling board.
</p>
<p>
Once the stack has been populated, long-press on a slate to initiate dragging.
Drag to the left and up to shuffle it to the top. Drag to the
right and down to shuffle it to the end.

Use these actions to bring your list into the correct order.
</p>
'
)
        }
    }
  }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
