// Copyright (c) 2022 Peter G. (nephros)
//
// SPDX-License-Identifier: MPL-2.0

import QtQuick 2.6
import QtQml.Models 2.3
import Sailfish.Silica 1.0
import Nemo.Notifications 1.0
import "../components"
import "../test/todo.txt.js" as TESTList
import "../js/Utils.js" as Utils
import "../js/todotxt.js" as TodoTxtUtil
import "../js/exportics.js" as ExportUtil

Page {
    id: page

    property ListModel todolist: ListModel{}
    allowedOrientations: Orientation.All

    Component.onCompleted: {
        orientation : Orientation.Landscape
        flick.scrollToBottom()
        taskList.scrollToBottom()
    }

    PullDownMenu { id: pdp
        flickable: flick
        MenuItem { text: qsTr("Help") + "/" + qsTr("About"); onClicked: { pageStack.push(Qt.resolvedUrl("AboutPage.qml")) } }
        MenuItem { text: qsTr("Settings"); onClicked: { pageStack.push(Qt.resolvedUrl("SettingsPage.qml")) } }
        MenuItem { text: qsTr("Load Todo List"); onClicked: {
            var dialog = pageStack.push(Qt.resolvedUrl("../dialogs/LoadList.qml"));
            dialog.accepted.connect(function() {
                //console.debug("Loading dislog result: " + dialog.outData);
                // do not freeze the animation while loading:
                pageStack.completeAnimation()
                loadTodoList(dialog.outData,false)
                // does not work???
                //Qt.callLater(page.loadTodoList,dialog.outData,false)
            }
            );
            }
        }
        MenuItem { text: qsTr("Load/Append Test File"); onDelayedClick: { loadTodoList(TESTList.list, true) } }
    }
    PushUpMenu { id: pup
        flickable: flick
        MenuItem { text: qsTr("Clear Board"); onDelayedClick: todolist.clear(); enabled: todolist.count > 0 }
        MenuItem { text: qsTr("Export Events"); enabled: todolist.count > 0
            onDelayedClick: {
                var files = exportTodoList();
                /* TODO:
                files.forEach(function(f) {
                    Qt.openUrlExternally(f);
                })
                */
            }
        }
        MenuItem { text: qsTr("Export Events (all-in-one)"); enabled: todolist.count > 0; onDelayedClick: { var files = exportTodoList(false); } }
    }

    SilicaFlickable {
        id: flick
        anchors.fill: parent
        contentHeight: taskList.height
        PageHeader { id: header ; clip: true; title: qsTr(Qt.application.name) + " " + qsTr("Scheduling Board") }

        SilicaListView { id: taskList
            width: parent.width - Theme.horizontalPageMargin
            anchors.top: header.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            height: page.height - (header.height)

            spacing: Theme.paddingSmall
            //verticalLayoutDirection: app.gravity ? ListView.BottomToTop : ListView.TopToBottom
            add:        Transition { FadeAnimation { duration: 1200 } }
            //move:     Transition { FadeAnimation { duration: 1200 } }
            displaced: Transition {id: dispTrans
                SequentialAnimation {
                  property int commonDuration: 600
                  PauseAnimation {
                    duration: (dispTrans.ViewTransition.index - dispTrans.ViewTransition.targetIndexes[0]) * parent.commonDuration *2
                  }
                  NumberAnimation { properties: "y"; duration: parent.commonDuration; easing.type: Easing.OutQuart; }
            }
            }
            move: Transition { NumberAnimation { properties: "y"; easing.type: Easing.OutQuart; duration: 600 } }
            //displaced:  Transition { NumberAnimation { properties: "x,y"; duration: 900} }
            //populate:   Transition { FadeAnimation { duration: 1200 } }
            //populate:  Transition { NumberAnimation { properties: "height"; from: height/2; duration: 2000} }
            //populate: Transition {id: popTrans
            //    SequentialAnimation { 
            //    PauseAnimation { duration: (popTrans.ViewTransition.index - popTrans.ViewTransition.targetIndexes[0]) * 100 }
            //    NumberAnimation { properties: "y"; from:0; easing.type: Easing.OutQuart; duration: 1200 }
            //}
            //}
            clip: true
            model: todolist
            delegate: FlightStrip { id: taskDelegate
                width: ListView.view.width

                //task: todolist.get(index)
                task: model
                menu: Component { ContextMenu {
                    //MenuItem { enabled: false; text: "Remove"; onClicked: remove() }
                    MenuItem { text: "Export";
                        onDelayedClick: {
                            const path = Qt.resolvedUrl(StandardPaths.Documents);
                            const vtodo = elementToVTODO(todolist.get(index));
                            const file = ExportUtil.putTodo(vtodo,path);
                            console.debug("Exported TODO " + index + " to " + file + ".");
                        }
                    }
                    //MenuItem { enabled: false; text: "Edit"; onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/TodoEdit.qml", task)) }
                }}
                //openMenuOnPressAndHold: (drag.target || dragged) ? false : true
                openMenuOnPressAndHold: false


                /*
                 *  ********** Drag and Drop stuff ***********
                 */
                enabled: !taskList.busy

                property int dragThreshold: width / 7
                property int shuffleThreshold: height //* 1.5
                property var pressPosition
                property int oldIndex: index

                onPressed: {
                    pressPosition = Qt.point(mouse.x, mouse.y)
                    taskDelegate.grabToImage(function(result) { dragImage.url = result.url; });
                    taskDelegate.oldIndex = index
                    taskDelegate.dragged = true
                }

                onReleased: reset()
                onCanceled: reset()

                onPositionChanged: {
                    var deltaX = pressPosition.x - mouse.x
                    if (!drag.target) {
                        if ((deltaX > dragThreshold) || (-deltaX > dragThreshold)) {
                            var newPos = mapToItem(taskList.contentItem, mouse.x, mouse.y)
                            content.parent = taskList.contentItem
                            content.x = newPos.x - pressPosition.x
                            content.y = newPos.y - pressPosition.y
                            drag.target = content
                        } else if (deltaX > 0) {
                            content.x = -deltaX
                        } else {
                            content.x = 0
                        }
                    }
                }

                function shuffleUp()   { if (drag.target) { todolist.move(index, 0, 1) } }
                function shuffleDown() { if (drag.target) { todolist.move(index, todolist.count-1, 1) } }

                function reset() {
                    if (!drag.target) {
                        content.x = 0
                        taskDelegate.dragged = false
                        return
                    }
                    drag.target = null
                    var ctod = content.mapToItem(taskDelegate, content.x, content.y)
                    ctod.x = ctod.x - content.x
                    ctod.y = ctod.y - content.y
                    content.parent = taskDelegate
                    content.x = ctod.x
                    content.y = ctod.y
                    taskDelegate.dragged = false
                    backAnimation.start()
                }

                Rectangle {
                    id: content
                    Image { id: dragImage
                        property url url
                        source: taskDelegate.drag.target ? url : ""
                        smooth: false
                    }
                    width: parent.width
                    height: Theme.itemSizeSmall
                    color: taskDelegate.drag.target ? Theme.rgba(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity / 2)
                        : "transparent"
                    border.width: taskDelegate.drag.target ? 2 : 0
                    border.color: taskDelegate.drag.target ? Theme.rgba(Theme.highlightdColor, Theme.highlightBackgroundOpacity)
                        : "transparent"

                    onYChanged: {
                        if (!taskDelegate.drag.target) {
                            return
                        }

                        const deltaX = pressPosition.x - taskDelegate.mouseX
                        const deltaY = pressPosition.y - taskDelegate.mouseY
                        if ((deltaY > shuffleThreshold) && (deltaX > 0)) {
                            shuffleUp()
                            return
                        }
                        if ((-deltaY > shuffleThreshold) && (deltaX < 0)) {
                            shuffleDown()
                            return
                        }
                    }

                    NumberAnimation {
                        id: backAnimation
                        target: content
                        properties: "x,y"
                        to: 0
                        duration: 400
                        easing.type: Easing.OutQuart;
                    }

                }

                /* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                 *  ********** Drag and Drop stuff ***********
                 */

            }

        ViewPlaceholder {
                enabled: taskList.count == 0
                text: qsTr("No tasks yet")
                hintText: qsTr("Pull down to add tasks")
            }
        }
        RemorsePopup { id: applyRemorse }
        Notification { id: notification; isTransient: true; appName: qsTr(Qt.application.name); category: "transfer"}
        VerticalScrollDecorator {}
    }

    function loadTodoList(list, append) {
        console.time("loading took:");

        // default to false if parameter is undefined
        append = (typeof append !== 'undefined') ?  append : false


        console.time("parsing took:");
        var todos = TodoTxtUtil.TodoTxt.parseFile(list);
        console.timeEnd("parsing took:");
        // Access properties on the new items.
        console.info("Loaded " + todos.length + " Todos");

        // Fetch items from the object
        var items = todos.items();

        // clear list
        if (!append) {
            console.time("clearing took:");
            page.todolist.clear();
            console.timeEnd("clearing took:");
        }

        console.time("transforming took:");
        const e;
        // define template outside the loop
        const tpl = {
            "finished":          false,
            "prio":              "",
            "completionDate":    0,
            "creationDate":      0,
            "dueDate":           0,
            "content":           "",
            "contexts":          { "items": [] },
            "projects":          { "items": [] },
            "addons":            { }
        };
        for ( var i=0 ;i < todos.length; i++) {
            if ( app.loadfinished && e.isComplete()) {
                continue;
            }
            e = items[i];
            const o = tpl;

            //console.debug("Looking at: ", e.render());
            o.finished          = e.isComplete();
            o.prio              = e.priority();                   // ==> B
            o.completionDate    = (e.completedDate() != null) ? e.completedDate().getTime()/1000 : 0
            o.creationDate      = (e.createdDate() != null)   ? e.createdDate().getTime()/1000   : 0
            o.content           = e.textTokens().join(' ');    // ==> ['Write', 'a', 'new', 'song']
            o.contexts.items    = e.contexts();                 // ==> ['@guitar', '@home']
            o.projects.items    = e.projects();                 // ==> ['+music']
            const a = e.addons();         // ==> {due: '2014-04-15', for: ['me','wife']}
            //console.debug("addons for '" + o.content + "':", JSON.stringify(a,null,2));
            // extract due date
            if (a.hasOwnProperty('due')) {
                const d = a['due'].split('-');
                const dd = new Date(d[0], d[1] -1, d[2], 0,0,0,0);
                if (isNaN(dd.getTime())) {
                    console.warn("WARNING: Due date set but invalid!");
                } else {
                    //console.debug("Due date is valid.");
                    o.dueDate = dd.getTime()/1000;
                }
                delete a.due;
            }
            // generate and add a callsign:
            a["callsign"] = Utils.getCallsign();

            // add remaining addons:
            o.addons = a;
            page.todolist.append(o);
            //console.debug("Final Result:", JSON.stringify(o,null,2));
            //page.todolist.set(i,o);
            //console.time("transforming " + i + ":");
        };
        console.timeEnd("transforming took:");
        //stripModel.populate();

        /*
            console.log(items[0].lineNumber());     // ==> 1
            console.log(items[0].id());             // ==> (a UUID-like string)
            console.log(items[0].contexts());       // ==> ['@guitar', '@home']
            console.log(items[0].projects());       // ==> ['+music']
            console.log(items[0].priority());       // ==> null
            console.log(items[0].createdDate());    // ==> null
            console.log(items[0].isComplete());     // ==> false
            console.log(items[0].completedDate());  // ==> null
            console.log(items[0].addons());         // ==> {}
            console.log(items[0].textTokens());     // ==> ['Write', 'a', 'new', 'song']

            console.log(items[1].lineNumber());     // ==> 2
            console.log(items[1].contexts());       // ==> ['@bicycle']
            console.log(items[1].projects());       // ==> ['+stayhealthy']
            console.log(items[1].priority());       // ==> B
            console.log(items[1].createdDate());    // ==> Date object (April 10, 2014)
            console.log(items[1].isComplete());     // ==> false
            console.log(items[1].completedDate());  // ==> null
            console.log(items[1].addons());         // ==> {}

            console.log(items[2].lineNumber());     // ==> 3
            console.log(items[2].contexts());       // ==> ['@grocerystore']
            console.log(items[2].projects());       // ==> []
            console.log(items[2].priority());       // ==> null
            console.log(items[2].createdDate());    // ==> null
            console.log(items[2].isComplete());     // ==> true
            console.log(items[2].completedDate());  // ==> Date object (March 2, 2014)
            console.log(items[2].addons());         // ==> {}

            console.log(items[3].lineNumber());     // ==> 4
            console.log(items[3].contexts());       // ==> []
            console.log(items[3].projects());       // ==> []
            console.log(items[3].priority());       // ==> 'A'
            console.log(items[3].createdDate());    // ==> null
            console.log(items[3].isComplete());     // ==> false
            console.log(items[3].completedDate());  // ==> null
            console.log(items[3].addons());         // ==> {due: '2014-04-15', for: ['me','wife']}
        */
        console.timeEnd("loading took:");
    }

    /*
     * Transform todolist model into one or more iCalendar files
     *
     * multiple: if false, saves one file and returns its name/url
     * multiple: if true, saves each todo in a separate file and returns a list of filenames/urls
     */
    function exportTodoList(multiple) {
        // default to true if parameter is undefined
        multiple = (typeof multiple !== 'undefined') ?  multiple : true
        const path = Qt.resolvedUrl(StandardPaths.temporary);

        const files = []; // list of all generated files
        const all = [];   // collector for each VTODO element
        for (var i=0; i<todolist.count; i++) {
            const data = ExportUtil.elementToVTODO(todolist.get(i));
            // export as separate files or collect to save later
            (multiple) ? files.push(ExportUtil.putTodo(data,path)) : all.push(data);
        }
        // all in one
        if (!multiple) { files.push(ExportUtil.putTodo(all.join(''),path)); }
        console.info("Exported " + files.length + " iCalendar file(s) containing " + todolist.count + " events.");
        notification.previewSummary = qsTr("Exported %n event(s).", "", todolist.count);
        notification.previewBody = qsTr("Created %n iCalendar file(s): \n","", files.length) + files.join('\n');
        notification.publish();
        return files;
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
